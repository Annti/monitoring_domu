package sample;

import java.io.IOException;

/**
 * Created by Slow on 2016-04-06.
 */
public interface Command {
    void execute() throws IOException;
}
