package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Slow on 2016-04-06.
 */
public class LoginLayoutCommand implements Command {
    LoginLayoutBuilder loginLayoutBuilder = new LoginLayoutBuilder();
    LoginLayoutCommand(BorderPane borderPane){
        this.loginLayoutBuilder.borderPane =  borderPane;
    }
    @Override
    public void execute() throws IOException {
        URL menuBarUrl = getClass().getResource("TopMenu.fxml");
        MenuBar bar = FXMLLoader.load( menuBarUrl );
        this.loginLayoutBuilder.borderPane.setTop(bar);
        URL paneOneUrl = getClass().getResource("LoginScene.fxml");
        AnchorPane paneOne = FXMLLoader.load( paneOneUrl );
        this.loginLayoutBuilder.borderPane.setCenter(paneOne);
    }
}
