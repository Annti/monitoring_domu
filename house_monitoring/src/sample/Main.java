package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        LayoutDirector layoutDirector = new LayoutDirector();
        primaryStage.setTitle("Hello World");

        Scene loginScene = new Scene(layoutDirector.createLoginScene());
        primaryStage.setScene(loginScene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
