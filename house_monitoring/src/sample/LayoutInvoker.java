package sample;

import java.io.IOException;

/**
 * Created by Slow on 2016-04-06.
 */
public class LayoutInvoker {
    public Command command;

    public LayoutInvoker(Command c){
        this.command=c;
    }

    public void execute() throws IOException {
        this.command.execute();
    }
}
