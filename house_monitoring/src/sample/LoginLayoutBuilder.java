package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Slow on 2016-04-05.
 */
public class LoginLayoutBuilder implements LayoutBuilderInterface {

    BorderPane borderPane = new BorderPane();
    @Override
    public void setLayout() throws IOException {
       LoginLayoutCommand loginLayoutCommand = new LoginLayoutCommand(this.borderPane);
        LayoutInvoker layoutInvoker = new LayoutInvoker(loginLayoutCommand);
        layoutInvoker.execute();
    }

    @Override
    public BorderPane getBorderPane() {
        return this.borderPane;
    }
}
