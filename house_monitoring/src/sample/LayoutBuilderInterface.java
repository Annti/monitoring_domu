package sample;

import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * Created by Slow on 2016-04-05.
 */
public interface LayoutBuilderInterface {
    void setLayout() throws IOException;
    BorderPane getBorderPane();
}
