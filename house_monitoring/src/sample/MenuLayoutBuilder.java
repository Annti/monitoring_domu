package sample;

import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * Created by Slow on 2016-04-05.
 */
public class MenuLayoutBuilder implements LayoutBuilderInterface {

    BorderPane borderPane = new BorderPane();

    @Override
    public void setLayout() throws IOException {

    }

    @Override
    public BorderPane getBorderPane() {
        return this.borderPane;
    }
}
