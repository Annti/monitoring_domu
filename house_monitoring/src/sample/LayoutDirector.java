package sample;

import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * Created by Slow on 2016-04-05.
 */
public class LayoutDirector {
    public BorderPane createLoginScene() throws IOException {
        LayoutBuilderInterface lbi = new LoginLayoutBuilder();
        lbi.setLayout();
    return lbi.getBorderPane();
    }

}
